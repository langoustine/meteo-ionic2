/**
 * Created by Vince on 09/07/2017.
 */

import {Component} from "@angular/core";

@Component({
  templateUrl: './large-brick.html',
  selector: 'large-brick'

})

export class LargeBrick {

  private weatherData:any = null;
  private iconLabel:string;
  private time = "Aujourd'hui";
  private date = null;

  constructor(){

  }

  getData(data) {
    if(data) {

      this.weatherData = data;
      this.date = this.getWeekDay(this.weatherData.currently.time);
      switch(this.weatherData.currently.icon) {
        case "cloudy": {
          this.iconLabel = "cloudy";
          break;
        }
        case "fog" : {
          this.iconLabel = "cloudy";
          break;
        }
        case "wind" : {
          this.iconLabel = "cloudy";
          break;
        }
        case "partly-cloudy-night": {
          this.iconLabel = "cloudy-night";
          break;
        }
        case "clear-night": {
          this.iconLabel = "moon";
          break;
        }
        case "rain": {
          this.iconLabel = "rain";
          break;
        }
        case "snow": {
          this.iconLabel = "snow";
          break;
        }
        case "sleet": {
          this.iconLabel = "snow";
          break;
        }
        case "partly-cloudy-day": {
          this.iconLabel = "partly-sunny";
          break;
        }
        case "clear-day": {
          this.iconLabel = "sunny";
          break;
        }
        case "thunderstorm": {
          this.iconLabel = "thunderstorm";
          break;
        }
      }
    }
  }

  getWeekDay(time){
    let d = new Date(time*1000);
    let weekday = new Array(7);
    weekday[0] = "Dimanche";
    weekday[1] = "Lundi";
    weekday[2] = "Mardi";
    weekday[3] = "Mercredi";
    weekday[4] = "Jeudi";
    weekday[5] = "Vendredi";
    weekday[6] = "Samedi";
    let n = "\n"+ weekday[d.getDay()] +" "+ d.getDate();
    return n;
  }
}

