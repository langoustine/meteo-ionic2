import {Component, Input, ViewChild} from '@angular/core';
import { Platform } from 'ionic-angular';
import { NavController, ModalController, ToastController } from 'ionic-angular';
import * as Constant from '../../app/constants';
import { Geolocation, Geoposition} from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderForwardResult, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';

import {DarkskyService} from '../../providers/darksky-service';

import {AutocompletePage} from '../auto-complete/auto-complete';
import {LargeBrick} from "../large-brick/large-brick";
import {SmallBricks} from "../small-brick/small-bricks";

/*
  Generated class for the SearchPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'home.html',
  providers: [DarkskyService]

})
export class HomePage {
  msg;
  meteo : any;
  private searchQuery: string = '';
  private items: string[];
  private current_lat=null;
  private current_long=null;
  coords : string

  @ViewChild('largeBrick') largebrick: LargeBrick;
  @ViewChild('smallBricks') smallBricks: SmallBricks;

  constructor(public plt: Platform, public darkskyService: DarkskyService, private navCtrl: NavController, private geolocation: Geolocation, private modalCtrl: ModalController, public geocoder: NativeGeocoder, public toaster: ToastController) {

    //this.initMeteo();
    this.msg="Veuillez selectionner une adresse";
    this.geolocation.getCurrentPosition().then((resp) => {
    this. current_lat = resp.coords.latitude.toString();
    this. current_long = resp.coords.longitude.toString();
    this.getMeteo(this. current_lat,this.current_long);
    console.log(this. current_lat, this. current_long);
      }).catch((error) => {
        console.log('Erreur pour vous localiser', error);
      });

  }

  getMeteo(lat,long){
  this.darkskyService.load(lat,long)
  .then(data=> {
    this.meteo = data;
    this.largebrick.getData(data);
    this.smallBricks.setInfo(data);
    console.log(data)
  });
}

  showAddressModal () {
    let modal = this.modalCtrl.create(AutocompletePage);
    let me = this;
    modal.onDidDismiss(data => {
      this.msg = "Changer d'adresse";
      this.getcoord(data);
    });
    modal.present();
  }


  getcoord(input) {
    var use_lat;
    var use_long;
      if (this.plt.is('cordova')) {
          this.geocoder.forwardGeocode(input)
    .then((coordinates: NativeGeocoderForwardResult) => {
          this.coords = "Lat=" + coordinates.latitude + " ; Long=" + coordinates.longitude;
          console.log("Lat=" + coordinates.latitude + " ; Long=" + coordinates.longitude);
          use_lat=coordinates.latitude;
          use_long=coordinates.longitude;

          let toast = this.toaster.create({
          message: this.coords,
        duration: 4000
      });
      toast.present();
        })
        .catch((error: any) => console.log(error));
    }else{
          console.log("Debug Web, utilisation de la localisation courante pour les coords")
          this.coords = "Lat=" + this.current_lat + " ; Long=" + this.current_long;
          console.log("Lat=" + this.current_lat  + " ; Long=" + this.current_long);
          use_lat=this.current_lat;
          use_long=this.current_long;
    }
    this.getMeteo(use_lat,use_long);

  }

/*
  initializeItems() {

    this.items = [
      'Paris',
      'Lyon',
      Constant.DARKSKY_API_KEY,
      Constant.PLACES_API_KEY,
      this.current_lat,
      this.current_long,
    ]
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.length) {
      this.items = this.items.filter((item) => {
        return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  geolocate() {
    let options = {
      enableHighAccuracy: true
    };

      this.geolocation.getCurrentPosition(options).then((position: Geoposition) => {
      this.getcountry(position);
    }).catch((err) => {
      alert(err);
    })

  }
 */

/*
  getcountry(pos) {
    this.geocoder.reverseGeocode(pos.coords.latitude, pos.coords.longitude).then((res: NativeGeocoderReverseResult) => {
      let country = this.toaster.create({
        message: pos.coords.latitude + '-' + pos.coords.longitude + '-' + res.countryName,
        duration: 4000
      });
      country.present();
    })
  }
*/


}
