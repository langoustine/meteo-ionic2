/**
 * Created by Vince on 09/07/2017.
 */

import {Component} from "@angular/core";
import {ToastController} from "ionic-angular";

@Component({
  templateUrl: './small-bricks.html',
  selector: 'small-bricks'

})

export class SmallBricks {
  private weeklyInfo: any[] = null;

  constructor(public toaster: ToastController){}

  setInfo(data) {
    if (data) {
      this.weeklyInfo = data.daily.data;
      this.updateIconLabel();
    }

    console.log(this.weeklyInfo[2]);

  }

  updateIconLabel() {
    this.weeklyInfo.shift();
    this.weeklyInfo.forEach((res, index) => {
      res.day = this.getWeekDay(res.time);
      switch (res.icon) {
        case "fog": {
          res.icon = "cloudy";
          break;
        }
        case "wind": {
          res.icon = "cloudy";
          break;
        }
        case "cloudy": {
          res.icon = "cloudy";
          break;
        }
        case "partly-cloudy-night": {
          res.icon = "cloudy-night";
          break;
        }
        case "clear-night": {
          res.icon = "moon";
          break;
        }
        case "rain": {
          res.icon = "rainy";
          break;
        }
        case "snow": {
          res.icon = "snow";
          break;
        }
        case "sleet": {
          res.icon = "snow";
          break;
        }
        case "partly-cloudy-day": {
          res.icon = "partly-sunny";
          break;
        }
        case "clear-day": {
          res.icon = "sunny";
          break;
        }
        case "thunderstorm": {
          res.icon = "thunderstorm";
          break;
        }
      }

    });
  }

  getWeekDay(time){
    let d = new Date(time*1000);
    let weekday = new Array(7);
    weekday[0] = "Dimanche";
    weekday[1] = "Lundi";
    weekday[2] = "Mardi";
    weekday[3] = "Mercredi";
    weekday[4] = "Jeudi";
    weekday[5] = "Vendredi";
    weekday[6] = "Samedi";
    let n = weekday[d.getDay()];
    return n;
  }

  getMoreInfo(data) {
    let toast = this.toaster.create({
      message: data.summary,
      duration: 6000
    });
    toast.present();
  }

}
