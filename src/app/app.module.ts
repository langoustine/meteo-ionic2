import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {AutocompletePage} from '../pages/auto-complete/auto-complete'
import {LargeBrick} from "../pages/large-brick/large-brick";
import {SmallBricks} from "../pages/small-brick/small-bricks";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AutocompletePage,
    LargeBrick,
    SmallBricks
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AutocompletePage,
    LargeBrick,
    SmallBricks
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    NativeGeocoder,

    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})

export class AppModule {}
